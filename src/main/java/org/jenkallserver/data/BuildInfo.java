package org.jenkallserver.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class BuildInfo {
    @JsonProperty("startedByUser")
    private Boolean startedByUser;
    @JsonProperty("emailAddress")
    private String emailAddress;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("projectNameWithBranch")
    private String projectNameWithBranch;
    @JsonProperty("linkToBuild")
    private String linkToBuild;
    @JsonProperty("jenkinsUrl")
    private String jenkinsUrl;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("parameters")
    private HashMap<String, String> parameters;
    @JsonProperty("relevantParameterSet")
    private Boolean relevantParameterSet;
    @JsonProperty("buildHasParameters")
    private Boolean buildHasParameters;
    @JsonProperty("startNotificationEnabled")
    private Boolean startNotificationEnabled;
    @JsonProperty("completeNotificationEnabled")
    private Boolean completeNotificationEnabled;
    @JsonProperty("beingOverwrittenEnabled")
    private Boolean beingOverwrittenEnabled;
    @JsonProperty("overwritingAnotherEnabled")
    private Boolean overwritingAnotherEnabled;
    @JsonProperty("result")
    private String result;
    @JsonProperty("buildNumber")
    private String buildNumber;
    @JsonProperty("runType")
    private String runType;
    @JsonIgnore
    private String teamsUserId;

    @NotNull
    public String getEmailAddress() {
        return emailAddress;
    }

    @NotNull
    public String getUserName() {
        return userName;
    }

    @NotNull
    public String getUserId() {
        return userId;
    }

    @NotNull
    public String getProjectNameWithBranch() {
        return projectNameWithBranch;
    }

    @NotNull
    public String getLinkToBuild() {
        return linkToBuild;
    }

    @NotNull
    public String getJenkinsUrl() {
        return jenkinsUrl;
    }

    @NotNull
    public String getTimestamp() {
        return timestamp;
    }

    @NotNull
    public HashMap<String, String> getParameters() {
        return parameters;
    }

    @NotNull
    public Boolean getRelevantParameterSet() {
        return relevantParameterSet;
    }

    @NotNull
    public Boolean getBuildHasParameters() {
        return buildHasParameters;
    }

    @NotNull
    public Boolean getStartNotificationEnabled() {
        return startNotificationEnabled;
    }

    @NotNull
    public Boolean getCompleteNotificationEnabled() {
        return completeNotificationEnabled;
    }

    @NotNull
    public Boolean getBeingOverwrittenEnabled() {
        return beingOverwrittenEnabled;
    }

    @NotNull
    public Boolean getOverwritingAnotherEnabled() {
        return overwritingAnotherEnabled;
    }

    @NotNull
    public String getResult() {
        return result;
    }

    @NotNull
    public String getBuildNumber() {
        return buildNumber;
    }

    @NotNull
    public String getRunType() {
        return runType;
    }

    @NotNull
    public String getTeamsUserId() {
        return teamsUserId;
    }

    public void setTeamsUserId(String teamsUserId) {
        this.teamsUserId = teamsUserId;
    }

    @Override
    public String toString() {
        return "BuildInfo{" +
                "startedByUser=" + startedByUser +
                ", emailAddress='" + emailAddress + '\'' +
                ", userName='" + userName + '\'' +
                ", userId='" + userId + '\'' +
                ", projectNameWithBranch='" + projectNameWithBranch + '\'' +
                ", linkToBuild='" + linkToBuild + '\'' +
                ", jenkinsUrl='" + jenkinsUrl + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", parameters=" + parameters +
                ", relevantParameterSet=" + relevantParameterSet +
                ", buildHasParameters=" + buildHasParameters +
                ", startNotificationEnabled=" + startNotificationEnabled +
                ", completeNotificationEnabled=" + completeNotificationEnabled +
                ", beingOverwrittenEnabled=" + beingOverwrittenEnabled +
                ", overwritingAnotherEnabled=" + overwritingAnotherEnabled +
                ", result='" + result + '\'' +
                ", buildNumber='" + buildNumber + '\'' +
                ", runType='" + runType + '\'' +
                ", teamsUserId='" + teamsUserId + '\'' +
                '}';
    }
}

