package org.jenkallserver.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.HashMap;

public class ProjNameWithParams implements Serializable {

    @JsonProperty("projectName")
    private String projectName;

    @JsonProperty("parameters")
    private HashMap<String, String> parameters;

    public ProjNameWithParams(BuildInfo buildInfo) {
        this.projectName = buildInfo.getProjectNameWithBranch();
        this.parameters = buildInfo.getParameters();
    }

    @JsonProperty("projectName")
    public String getProjectName() {
        return projectName;
    }

    @JsonProperty("projectName")
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @JsonProperty("parameters")
    public HashMap<String, String> getParameters() {
        return parameters;
    }

    @JsonProperty("parameters")
    public void setParameters(HashMap<String, String> parameters) {
        this.parameters = parameters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProjNameWithParams pb)) return false;
        return pb.projectName.equals(projectName) && pb.parameters.equals(parameters);
    }

    @Override
    public int hashCode() {
        return projectName.hashCode() + parameters.hashCode();
    }
}
