package org.jenkallserver.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Starter implements Serializable {

    @JsonProperty("userName")
    private String userName;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("teamsUserId")
    private String teamsUserId;
    @JsonProperty("linkToBuild")
    private String linkToBuild;
    @JsonProperty("jenkinsUrl")
    private String jenkinsUrl;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("startNotificationEnabled")
    private Boolean startNotificationEnabled;
    @JsonProperty("completeNotificationEnabled")
    private Boolean completeNotificationEnabled;
    @JsonProperty("beingOverwrittenEnabled")
    private Boolean beingOverwrittenEnabled;
    @JsonProperty("overwritingAnotherEnabled")
    private Boolean overwritingAnotherEnabled;
    @JsonProperty("buildNumber")
    private String buildNumber;
    @JsonProperty("projectNameWithBranch")
    private String projectNameWithBranch;

    @SuppressWarnings("unused")
    public Starter() {
        this.userName = null;
        this.userId = null;
        this.teamsUserId = null;
        this.linkToBuild = null;
        this.jenkinsUrl = null;
        this.timestamp = null;
        this.startNotificationEnabled = null;
        this.completeNotificationEnabled = null;
        this.beingOverwrittenEnabled = null;
        this.overwritingAnotherEnabled = null;
        this.buildNumber = null;
        this.projectNameWithBranch = null;
    }

    public Starter(BuildInfo buildInfo) {
        this.userName = buildInfo.getUserName();
        this.userId = buildInfo.getUserId();
        this.teamsUserId = buildInfo.getTeamsUserId();
        this.linkToBuild = buildInfo.getLinkToBuild();
        this.jenkinsUrl = buildInfo.getJenkinsUrl();
        this.timestamp = buildInfo.getTimestamp();
        this.startNotificationEnabled = buildInfo.getStartNotificationEnabled();
        this.completeNotificationEnabled = buildInfo.getCompleteNotificationEnabled();
        this.beingOverwrittenEnabled = buildInfo.getBeingOverwrittenEnabled();
        this.overwritingAnotherEnabled = buildInfo.getOverwritingAnotherEnabled();
        this.buildNumber = buildInfo.getBuildNumber();
        this.projectNameWithBranch = buildInfo.getProjectNameWithBranch();
    }

    @JsonProperty("userName")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("userName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("teamsUserId")
    public String getTeamsUserId() {
        return teamsUserId;
    }

    @JsonProperty("teamsUserId")
    public void setTeamsUserId(String teamsUserId) {
        this.teamsUserId = teamsUserId;
    }

    @JsonProperty("linkToBuild")
    public String getLinkToBuild() {
        return linkToBuild;
    }

    @JsonProperty("linkToBuild")
    public void setLinkToBuild(String linkToBuild) {
        this.linkToBuild = linkToBuild;
    }

    @JsonProperty("jenkinsUrl")
    public String getJenkinsUrl() {
        return jenkinsUrl;
    }

    @JsonProperty("jenkinsUrl")
    public void setJenkinsUrl(String jenkinsUrl) {
        this.jenkinsUrl = jenkinsUrl;
    }

    @JsonProperty("timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("startNotificationEnabled")
    public Boolean getStartNotificationEnabled() {
        return startNotificationEnabled;
    }

    @JsonProperty("startNotificationEnabled")
    public void setStartNotificationEnabled(Boolean isStartNotificationEnabled) {
        this.startNotificationEnabled = isStartNotificationEnabled;
    }

    @JsonProperty("completeNotificationEnabled")
    public Boolean getCompleteNotificationEnabled() {
        return completeNotificationEnabled;
    }

    @JsonProperty("completeNotificationEnabled")
    public void setCompleteNotificationEnabled(Boolean isCompleteNotificationEnabled) {
        this.completeNotificationEnabled = isCompleteNotificationEnabled;
    }

    @JsonProperty("beingOverwrittenEnabled")
    public Boolean getBeingOverwrittenEnabled() {
        return beingOverwrittenEnabled;
    }

    @JsonProperty("beingOverwrittenEnabled")
    public void setBeingOverwrittenEnabled(Boolean isBeingOverwrittenEnabled) {
        this.beingOverwrittenEnabled = isBeingOverwrittenEnabled;
    }

    @JsonProperty("overwritingAnotherEnabled")
    public Boolean getOverwritingAnotherEnabled() {
        return overwritingAnotherEnabled;
    }

    @JsonProperty("overwritingAnotherEnabled")
    public void setOverwritingAnotherEnabled(Boolean isOverwritingAnotherEnabled) {
        this.overwritingAnotherEnabled = isOverwritingAnotherEnabled;
    }

    @JsonProperty("buildNumber")
    public String getBuildNumber() {
        return buildNumber;
    }

    @JsonProperty("buildNumber")
    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    @JsonProperty("projectNameWithBranch")
    public String getProjectNameWithBranch() {
        return projectNameWithBranch;
    }

    @JsonProperty("projectNameWithBranch")
    public void setProjectNameWithBranch(String projectNameWithBranch) {
        this.projectNameWithBranch = projectNameWithBranch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Starter s)) return false;
        return s.userName.equals(userName) &&
                s.userId.equals(userId) &&
                s.teamsUserId.equals(teamsUserId) &&
                s.linkToBuild.equals(linkToBuild) &&
                s.jenkinsUrl.equals(jenkinsUrl) &&
                s.timestamp.equals(timestamp) &&
                s.startNotificationEnabled.equals(startNotificationEnabled) &&
                s.completeNotificationEnabled.equals(completeNotificationEnabled) &&
                s.beingOverwrittenEnabled.equals(beingOverwrittenEnabled) &&
                s.overwritingAnotherEnabled.equals(overwritingAnotherEnabled) &&
                s.buildNumber.equals(buildNumber) &&
                s.projectNameWithBranch.equals(projectNameWithBranch);
    }

    @Override
    public int hashCode() {
        return userName.hashCode() +
                userId.hashCode() +
                teamsUserId.hashCode() +
                linkToBuild.hashCode() +
                jenkinsUrl.hashCode() +
                timestamp.hashCode() +
                startNotificationEnabled.hashCode() +
                completeNotificationEnabled.hashCode() +
                beingOverwrittenEnabled.hashCode() +
                overwritingAnotherEnabled.hashCode() +
                buildNumber.hashCode() +
                projectNameWithBranch.hashCode();
    }
}
