package org.jenkallserver.controller;


import org.jenkallserver.data.BuildInfo;
import org.jenkallserver.data.ProjNameWithParams;
import org.jenkallserver.data.Starter;
import org.jenkallserver.service.BuildTracker;
import org.jenkallserver.service.TeamsService;
import org.jenkallserver.util.JsonToObject;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class WebhookController {
    private static final String INITIALIZE = "INITIALIZE", COMPLETED = "COMPLETED";
    private final TeamsService teamsService;
    private final BuildTracker buildTracker;
    private static final Logger logger = java.util.logging.Logger.getLogger(WebhookController.class.getName());

    @Autowired
    public WebhookController(TeamsService teamsService, BuildTracker buildTracker) {
        this.teamsService = teamsService;
        this.buildTracker = buildTracker;
    }

    @PostMapping("/")
    public ResponseEntity<String> handleWebhook(@RequestBody String body) {

        BuildInfo buildInfo = JsonToObject.convert(body);

        if (buildInfo == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Could not parse JSON");
        }

        logger.info("Received webhook: " + buildInfo);

        String teamsUserId = teamsService.getUserIdFromEmail(buildInfo.getEmailAddress());

        if (teamsUserId == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Graph API Error");
        } else if (teamsUserId.equals("")) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Could not find any user on teams with email :" + buildInfo.getEmailAddress());
        }

        buildInfo.setTeamsUserId(teamsUserId);

        Starter starter = new Starter(buildInfo);
        ProjNameWithParams projNameWithParams = new ProjNameWithParams(buildInfo);

        // If the build has parameters and the Jenkall relevant parameter is not set,
        // then we assume that the user has not set up the Jenkall Build Tracker so,
        // only start and complete notifications are sent
        boolean haveToTrackBuild = !(buildInfo.getBuildHasParameters() && !buildInfo.getRelevantParameterSet());

        if (haveToTrackBuild) {
            if (buildInfo.getRunType().equals(INITIALIZE)) {
                List<Starter> oldStarters = buildTracker.alreadyRunningUsers(projNameWithParams);
                if (!oldStarters.isEmpty()) {
                    return responseEntityFromTeamsNotification(
                            teamsService.sendBuildAlreadyRunningNotificationSuccessfully(oldStarters, starter));
                }
                buildTracker.addBuild(projNameWithParams, starter);
            } else if (buildInfo.getRunType().equals(COMPLETED)) {
                buildTracker.removeBuild(projNameWithParams, starter);
            }
        }

        if (!buildInfo.getRunType().equals(INITIALIZE)) {
            return responseEntityFromTeamsNotification(
                    teamsService.sendBuildStatusNotificationSuccessfully(buildInfo));
        }

        return ResponseEntity.ok().build();
    }

    @NotNull
    private ResponseEntity<String> responseEntityFromTeamsNotification(boolean teamsServiceResponseOK) {
        if (teamsServiceResponseOK) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Graph API Error");
        }
    }
}
