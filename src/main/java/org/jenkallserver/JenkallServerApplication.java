package org.jenkallserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkallServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(JenkallServerApplication.class, args);
    }
}
