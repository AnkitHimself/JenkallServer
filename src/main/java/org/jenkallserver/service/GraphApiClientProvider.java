package org.jenkallserver.service;

import com.azure.identity.UsernamePasswordCredential;
import com.azure.identity.UsernamePasswordCredentialBuilder;
import com.microsoft.graph.authentication.TokenCredentialAuthProvider;
import com.microsoft.graph.requests.GraphServiceClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Service
public class GraphApiClientProvider {

    private final String clientId;
    private final String tenantId;
    private final List<String> graphUserScopes;
    private final String username;
    private final String password;
    private GraphServiceClient<Request> graphClient;

    public GraphApiClientProvider(
            @Value("${app.clientId}") String clientId,
            @Value("${app.tenantId}") String tenantId,
            @Value("${app.scopes}") String graphUserScopes,
            @Value("${app.USERNAME}") String username,
            @Value("${app.PASSWORD}") String password) {
        this.clientId = clientId;
        this.tenantId = tenantId;
        this.graphUserScopes = Arrays.asList(graphUserScopes.split(","));
        this.username = username;
        this.password = password;
    }

    @PostConstruct
    public void initGraphClient() {
        final UsernamePasswordCredential usernamePasswordCredential = new UsernamePasswordCredentialBuilder()
                .clientId(clientId).tenantId(tenantId).username(username).password(password)
                .build();

        final TokenCredentialAuthProvider authProvider = new TokenCredentialAuthProvider(
                graphUserScopes, usernamePasswordCredential);

        try {
            graphClient = GraphServiceClient.builder().authenticationProvider(authProvider).buildClient();
        } catch (Exception e) {
            throw new RuntimeException("Failed to create graph client", e);
        }
    }

    @Bean(name = "graphClient")
    public GraphServiceClient<Request> getGraphClient() {
        return graphClient;
    }
}

