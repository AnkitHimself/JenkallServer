package org.jenkallserver.service;

import com.google.gson.JsonPrimitive;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.models.*;
import com.microsoft.graph.requests.ConversationMemberCollectionPage;
import com.microsoft.graph.requests.ConversationMemberCollectionResponse;
import com.microsoft.graph.requests.GraphServiceClient;
import okhttp3.Request;
import org.jenkallserver.data.BuildInfo;
import org.jenkallserver.data.Starter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import static org.jenkallserver.util.ChatUtil.*;


@Service
public class TeamsService {

    private static final String STARTED = "STARTED", COMPLETED = "COMPLETED";
    private final GraphServiceClient<Request> graphClient;
    private final String myUserId;
    static Logger logger = Logger.getLogger(TeamsService.class.getName());

    @Autowired
    public TeamsService(GraphServiceClient<Request> graphClient) {
        if (graphClient == null) throw new RuntimeException("Graph not initialized");
        this.graphClient = graphClient;
        try {
            User me = graphClient.me().buildRequest().get();
            if (me != null) {
                myUserId = me.id;
                logger.info("You are signed in as " + me.displayName);
            } else {
                throw new RuntimeException("Me endpoint failed, Current user is null");
            }
        } catch (ClientException e) {
            throw new RuntimeException("Me endpoint failed, ClientException: " + e.getMessage());
        }
    }

    // Required Permissions: User.ReadBasic.All
    public String getUserIdFromEmail(String email) {
        try {
            User user = graphClient.users(email).buildRequest().get();
            if (user != null) {
                return user.id;
            } else {
                logger.severe("Could not find any user with email: " + email);
                return "";
            }
        } catch (ClientException e) {
            logger.severe("While trying to get user with email " + email + ", ClientException: " + e.getMessage());
            return "";
        }
    }

    // Required Permissions: Chat.Create, Chat.ReadWrite
    private Chat createChat(String teamsUserId) {
        LinkedList<ConversationMember> membersList = new LinkedList<>();
        addMemberToChat(myUserId, membersList);
        addMemberToChat(teamsUserId, membersList);

        ConversationMemberCollectionResponse conversationMemberCollectionResponse = new ConversationMemberCollectionResponse();
        conversationMemberCollectionResponse.value = membersList;

        Chat chat = new Chat();
        chat.chatType = ChatType.ONE_ON_ONE;
        chat.members = new ConversationMemberCollectionPage(conversationMemberCollectionResponse, null);

        try {
            return graphClient.chats()
                    .buildRequest()
                    .post(chat);
        } catch (ClientException e) {
            logger.severe("While trying to create chat with teamsUserId: " + teamsUserId + ", ClientException: " + e.getMessage());
            return null;
        }
    }

    private void addMemberToChat(String teamsUserId, LinkedList<ConversationMember> membersList) {
        AadUserConversationMember members1 = new AadUserConversationMember();
        LinkedList<String> rolesList1 = new LinkedList<>();
        rolesList1.add("owner");
        members1.roles = rolesList1;
        members1.additionalDataManager().put("user@odata.bind", new JsonPrimitive("https://graph.microsoft.com/v1.0/users('" + teamsUserId + "')"));
        members1.additionalDataManager().put("@odata.type", new JsonPrimitive("#microsoft.graph.aadUserConversationMember"));
        membersList.add(members1);
    }

    public boolean sendBuildStatusNotificationSuccessfully(BuildInfo buildInfo) {
        // If the user has disabled the notification for this type of run, return
        if ((!buildInfo.getCompleteNotificationEnabled()) && (Objects.equals(buildInfo.getRunType(), COMPLETED))) {
            return true;
        } else if ((!buildInfo.getStartNotificationEnabled()) && (Objects.equals(buildInfo.getRunType(), STARTED))) {
            return true;
        }

        ChatMessage chatMessage = buildStatusMessage(buildInfo);

        Chat chat = createChat(buildInfo.getTeamsUserId());
        if (chat == null) {
            return false;
        }
        return sendMessageInChatSuccessfully(chat.id, chatMessage);
    }

    public boolean sendBuildAlreadyRunningNotificationSuccessfully(List<Starter> oldStarters, Starter newStarter) {
        for (Starter oldStarter : oldStarters) {
            if (!oldStarter.getBeingOverwrittenEnabled()) {
                continue;
            }
            ChatMessage newChatMessage = buildBeingOverwrittenMessage(oldStarter, newStarter);

            Chat beingOverwritten = createChat(oldStarter.getTeamsUserId());
            if (beingOverwritten == null) {
                continue;
            }
            sendMessageInChatSuccessfully(beingOverwritten.id, newChatMessage);
        }

        if (!newStarter.getOverwritingAnotherEnabled()) {
            return true;
        }
        ChatMessage runningChatMessage = buildOverwritingMessage(newStarter, oldStarters);

        Chat overwriting = createChat(newStarter.getTeamsUserId());
        if (overwriting == null) {
            return false;
        }
        return sendMessageInChatSuccessfully(overwriting.id, runningChatMessage);
    }

    // Required Permissions: ChatMessage.Send, Chat.ReadWrite
    private boolean sendMessageInChatSuccessfully(String chatId, ChatMessage chatMessage) {
        try {
            graphClient.chats(chatId).messages().buildRequest().post(chatMessage);
            return true;
        } catch (ClientException e) {
            logger.severe("Could not send message in chatId: " + chatId
                    + "\nGraph API returned an error: " + e.getMessage());
            return false;
        }
    }
}
