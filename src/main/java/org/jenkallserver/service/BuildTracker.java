package org.jenkallserver.service;

import org.jenkallserver.data.ProjNameWithParams;
import org.jenkallserver.data.Starter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

@Service
@EnableScheduling
public class BuildTracker {

    // CHECK_INTERVAL must be less than MAX_DOWN_TIME_PERMISSIBLE
    private static final int MAX_DOWN_TIME_PERMISSIBLE = 15 * 60 * 1000; // 15 minutes
    private static final int CHECK_INTERVAL = 60 * 1000; // 1 minute

    private static final String RUNNING_BUILDS_KEY = "runningBuilds";
    private static final String LAST_UPTIME = "lastUptime";
    private static final Logger logger = java.util.logging.Logger.getLogger(BuildTracker.class.getName());
    private final HashOperations<String, ProjNameWithParams, List<Starter>> hashOperations;
    private final ValueOperations<String, String> lastUptimeOperations;

    @Autowired
    public BuildTracker(RedisTemplate<String, Object> redisTemplate, RedisTemplate<String, String> redisTemplateLastUptime) {
        this.hashOperations = redisTemplate.opsForHash();
        this.lastUptimeOperations = redisTemplateLastUptime.opsForValue();
    }

    public void addBuild(ProjNameWithParams projNameWithParams, Starter starter) {
        List<Starter> users = alreadyRunningUsers(projNameWithParams);
        users.add(starter);
        saveBuild(projNameWithParams, users);
    }

    public void removeBuild(ProjNameWithParams projNameWithParams, Starter starter) {
        List<Starter> users = alreadyRunningUsers(projNameWithParams);
        users.remove(starter);
        if (users.isEmpty()) {
            hashOperations.delete(RUNNING_BUILDS_KEY, projNameWithParams);
        } else {
            saveBuild(projNameWithParams, users);
        }
    }

    public List<Starter> alreadyRunningUsers(ProjNameWithParams projNameWithParams) {
        return Objects.requireNonNullElseGet(hashOperations.get(RUNNING_BUILDS_KEY, projNameWithParams), ArrayList::new);
    }

    private void saveBuild(ProjNameWithParams pb, List<Starter> users) {
        hashOperations.put(RUNNING_BUILDS_KEY, pb, users);
    }

    private void updateLastUptime() {
        logger.info("Updating last uptime to " + System.currentTimeMillis());
        lastUptimeOperations.set(LAST_UPTIME, String.valueOf(System.currentTimeMillis()));
    }

    @Scheduled(fixedDelay = CHECK_INTERVAL)
    private void checkDifferenceInUptime() {
        String lastUptime = lastUptimeOperations.get(LAST_UPTIME);
        if (lastUptime == null) {
            updateLastUptime();
            return;
        }
        long currentTime = System.currentTimeMillis();
        long difference = currentTime - Long.parseLong(lastUptime);
        if (difference > MAX_DOWN_TIME_PERMISSIBLE) {
            updateLastUptime();
            logger.warning("Server was down for " + difference + " milliseconds. Clearing all running builds.");
            hashOperations.entries(RUNNING_BUILDS_KEY).clear();
        }
    }
}
