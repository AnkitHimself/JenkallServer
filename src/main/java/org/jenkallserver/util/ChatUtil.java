package org.jenkallserver.util;

import com.microsoft.graph.models.BodyType;
import com.microsoft.graph.models.ChatMessage;
import com.microsoft.graph.models.ChatMessageAttachment;
import com.microsoft.graph.models.ItemBody;
import org.jenkallserver.data.BuildInfo;
import org.jenkallserver.data.Starter;

import java.util.LinkedList;
import java.util.List;

public class ChatUtil {

    public static ChatMessage buildOverwritingMessage(Starter newStarter, List<Starter> oldStarters) {
        assert oldStarters.size() > 0 && oldStarters.get(0).getProjectNameWithBranch().equals(newStarter.getProjectNameWithBranch());
        String projectName = newStarter.getProjectNameWithBranch();

        String title = "Your Build #" + newStarter.getBuildNumber() + " of project: " + projectName + " will Overwrite other builds";

        StringBuilder messageToSend = new StringBuilder("<strong>" + projectName +
                "'s</strong> Build was already in execution, with the same parameters, by user(s): " +
                "<br><ul><strong>");

        for (Starter starter : oldStarters) {
            messageToSend.append("<blockquote><li>").append(starter.getUserName())
                    .append(": ").append("<a href=\"").append(starter.getLinkToBuild())
                    .append("\">Build #").append(starter.getBuildNumber()).append("</a>").append("<br>")
                    .append("Started at: ").append(starter.getTimestamp()).append("</li></blockquote>");
        }
        messageToSend.append("</strong></ul><br>");

        messageToSend.append("Your Build Info:<br>");
        messageToSend.append("<strong>Project Name:</strong> ").append(projectName).append("<br>")
                .append("<strong>Build Number:</strong> ").append(newStarter.getBuildNumber()).append("<br>")
                .append("<strong>Start Time:</strong> ").append(newStarter.getTimestamp()).append("<br>")
                .append("<strong><a href=\"").append(newStarter.getLinkToBuild()).append("\">Go to Build #").append(newStarter.getBuildNumber()).append("</a></strong><br><br>");

        messageToSend.append("<strong><a href=\"").append(newStarter.getJenkinsUrl()).append("user/").append(newStarter.getUserId()).append("/configure")
                .append("\">Change Notification Preferences</a></strong>");

        MessageBody message = new MessageBody(title, "", messageToSend.toString());
        return createMessage(message);
    }

    public static ChatMessage buildBeingOverwrittenMessage(Starter oldStarter, Starter newStarter) {
        assert oldStarter.getProjectNameWithBranch().equals(newStarter.getProjectNameWithBranch());
        String newUserName = newStarter.getUserName(), projectName = oldStarter.getProjectNameWithBranch();

        String title = "Your Build #" + oldStarter.getBuildNumber() + " of project: " + projectName + " will be Overwritten";

        String messageToSend = "<strong>" + newUserName + "</strong>" + " has started a new build of project <strong>" +
                projectName + "</strong> with the same parameters, while your build #" + oldStarter.getBuildNumber() + " is yet to finish.<br>" +

                newUserName + "'s Build Info:" +
                "<blockquote><strong><a href=\"" + newStarter.getLinkToBuild() + "\">Go to " + newUserName + "'s Build #" + newStarter.getBuildNumber() + "</a></blockquote></strong><br>" +

                "<strong><a href=\"" + oldStarter.getLinkToBuild() + "\">Go to your Build #" + oldStarter.getBuildNumber() + "</a></strong><br><br>" +

                "<strong><a href=\"" + oldStarter.getJenkinsUrl() + "user/" + oldStarter.getUserId() + "/configure" + "\">Change Notification Preferences</a></strong>";

        MessageBody message = new MessageBody(title, "", messageToSend);
        return createMessage(message);
    }

    public static ChatMessage buildStatusMessage(BuildInfo buildInfo) {
        String projectName = buildInfo.getProjectNameWithBranch();

        String title = "Build #" + buildInfo.getBuildNumber() + " " + buildInfo.getRunType() + " for project: " + projectName;

        String messageToSend = "<strong>Project Name:</strong> " + projectName + "<br>" +
                "<strong>Build Number:</strong> " + buildInfo.getBuildNumber() + "<br>" +
                "<strong>Result:</strong> " + buildInfo.getResult() + "<br>" +
                "<strong>Start Time:</strong> " + buildInfo.getTimestamp() + "<br>" +
                "<strong><a href=\"" + buildInfo.getLinkToBuild() + "\">Go to Build #" + buildInfo.getBuildNumber() + "</a></strong><br><br>" +

                "<strong><a href=\"" + buildInfo.getJenkinsUrl() + "user/" + buildInfo.getUserId() + "/configure" + "\">Change Notification Preferences</a></strong>";

        MessageBody message = new MessageBody(title, "", messageToSend);
        return createMessage(message);
    }

    public static ChatMessage createMessage(MessageBody message) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.subject = null;

        ItemBody body = new ItemBody();
        body.contentType = BodyType.HTML;
        body.content = "<attachment id=\"74d20c7f34aa4a7fb74e2b30004247c5\"></attachment>";
        chatMessage.body = body;

        LinkedList<ChatMessageAttachment> attachmentsList = new LinkedList<>();

        ChatMessageAttachment attachments = new ChatMessageAttachment();
        attachments.id = "74d20c7f34aa4a7fb74e2b30004247c5";
        attachments.contentType = "application/vnd.microsoft.card.thumbnail";
        attachments.contentUrl = null;
        attachments.content = message.toJson();
        attachments.name = null;
        attachments.thumbnailUrl = null;

        attachmentsList.add(attachments);
        chatMessage.attachments = attachmentsList;

        return chatMessage;
    }
}
