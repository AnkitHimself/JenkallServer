package org.jenkallserver.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jenkallserver.data.BuildInfo;

import java.util.logging.Logger;

public class JsonToObject {
    static Logger logger = Logger.getLogger(JsonToObject.class.getName());
    public static BuildInfo convert(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(json, BuildInfo.class);
        } catch (Exception e) {
            logger.severe("Error converting JSON to object: " + e.toString());
            return null;
        }
    }
}
