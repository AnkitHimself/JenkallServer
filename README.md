# JenkallServer

JenkallServer is a Java (Spring Boot) based application that serves as the central tracking and communication hub for the Jenkall system. It receives job status updates from the JenkallConnect Plugin integrated into Jenkins, maintains a list of running builds (using Redis for persistence), and sends notification alerts to Microsoft Teams personal chat based on user preferences.
[Link to JenkallConnect Plugin GitLab Repository](https://gitlab.com/AnkitHimself/jenkall-connect)

## Getting Started

To set up the JenkallServer, follow the steps mentioned below.

### Prerequisites

1. Register the application in Azure Active Directory to use the Microsoft Graph API.
2. Get the client ID and tenant ID from the registered application.
3. Obtain the necessary Graph API permissions for sending messages to Microsoft Teams chat.

### Application Properties

Create an `application.properties` file in the resource folder under `src`. Add the following properties:

```
app.clientId=YOUR_CLIENT_ID
app.tenantId=YOUR_TENANT_ID
app.scopes=ChatMessage.Send,Chat.Create,Chat.ReadWrite,User.Read,User.ReadBasic.All
app.USERNAME=USER_ACCOUNT_EMAIL
app.PASSWORD=<Password_to_the_above_account>

server.port=8090

redis.port=6379
redis.host=localhost
```

Replace the placeholders (`YOUR_CLIENT_ID`, `YOUR_TENANT_ID`, etc.) with your actual values.

## Microsoft Graph API and Redis Server Setup

To set up the JenkallServer, you'll need to configure the Microsoft Graph API for sending notification alerts to Microsoft Teams personal chat and set up a Redis Server for data persistence. Below are the guidelines to achieve this setup:

### Microsoft Graph API Setup:

1. **Register the Application in Azure Active Directory:**

   - Go to the [Azure portal](https://portal.azure.com) and sign in with your account credentials.
   - Navigate to "Azure Active Directory" and select "App registrations" under "Manage."
   - Click on "New registration" to register a new application.
   - Provide a name for the application, and choose the appropriate supported account types.
   - Click on "Register" to create the application.

2. **Retrieve the Client ID and Tenant ID:**

   - After registering the application, you will be redirected to the application overview page.
   - Note down the "Application (client) ID" as your Client ID.
   - Also, note down your "Directory (tenant) ID" as your Tenant ID.

3. **Configure Application Permissions:**

   - Navigate to "API permissions" under the "Manage" section of the registered application.
   - Click on "Add a permission" and select "Microsoft Graph."
   - Choose the necessary application permissions required for sending messages in Microsoft Teams personal chat (e.g., `ChatMessage.Send`, `Chat.Create`, `Chat.ReadWrite`, `User.Read`, `User.ReadBasic.All`).
   - Click on "Add permissions" to add the selected permissions to your application.

4. **Obtain an Access Token for Graph API:**

   - To obtain an access token for the Graph API, you can use the "UsernamePasswordCredential" authorization method or other suitable methods depending on your application requirements.
   - With the access token, your application will have the necessary permissions to send messages to users in Microsoft Teams personal chat.

### Redis Server Setup:

1. **Install and Configure Redis:**

   - Download and install Redis on the server where you plan to host the JenkallServer application. You can find the appropriate installation instructions for your server's operating system.
   - Once installed, open the Redis configuration file (e.g., `redis.conf`) and configure the Redis server settings as needed (e.g., specify the port number and enable password authentication if required).

2. **Start Redis Server:**

   - Launch the Redis server by executing the Redis server executable (e.g., `redis-server`).
   - If you have set up password authentication, make sure to provide the correct password during server startup.

3. **Connect JenkallServer to Redis:**

   - In the JenkallServer application code, specify the connection details to your Redis server. These details typically include the host address, port number, and password (if authentication is enabled).
   - Use a Redis client library in your Java (Spring Boot) application to interact with the Redis server and store running build data for persistence.

By following the above steps, you will have successfully configured the Microsoft Graph API for sending notification alerts to Microsoft Teams personal chat and set up a Redis Server for data persistence in the JenkallServer application. This integration will enable smooth communication between Jenkins, JenkallConnect Plugin, and JenkallServer, ensuring efficient tracking of build requests and timely notifications to users.


## Conclusion

JenkallServer effectively addresses the problem of build overwrites and manual status monitoring by serving as the central communication hub for the Jenkall system. It provides real-time notifications to users on Microsoft Teams, enhancing efficiency among team members.
